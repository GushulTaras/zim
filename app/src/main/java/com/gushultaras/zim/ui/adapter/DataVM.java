package com.gushultaras.zim.ui.adapter;

import androidx.databinding.ObservableField;

import com.gushultaras.zim.data.Data;

import java.util.Objects;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public class DataVM {

    public final ObservableField<String> pictureUrl = new ObservableField<>();
    public final ObservableField<String> title = new ObservableField<>();

    private Data data;

    public DataVM(Data data) {
        this.data = data;
        pictureUrl.set(data.getUrl());
        title.set(data.getTitle());
    }

    public Data getData() {
        return data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataVM dataVM = (DataVM) o;
        return Objects.equals(data, dataVM.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }
}

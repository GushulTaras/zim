package com.gushultaras.zim.ui.adapter;

import androidx.recyclerview.widget.DiffUtil;

import java.util.List;
import java.util.Objects;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public class DiffUtilCallback extends DiffUtil.Callback {

    private final List<DataVM> oldList;
    private final List<DataVM> newList;

    public DiffUtilCallback(List<DataVM> oldList, List<DataVM> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return Objects.equals(oldList.get(oldItemPosition).getData().getTitle(), newList.get(newItemPosition).getData().getTitle());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return Objects.equals(oldList.get(oldItemPosition).getData(), newList.get(newItemPosition).getData());
    }
}

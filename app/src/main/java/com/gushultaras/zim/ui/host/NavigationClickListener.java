package com.gushultaras.zim.ui.host;

import com.gushultaras.zim.data.Data;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public interface NavigationClickListener {

    void onNavigationClick(Data data);

}

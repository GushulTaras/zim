package com.gushultaras.zim.ui.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.gushultaras.zim.databinding.ItemDataBinding;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public class DataVH extends RecyclerView.ViewHolder {

    private ItemDataBinding binding;

    public DataVH(@NonNull View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
    }

    public ItemDataBinding getBinding() {
        return binding;
    }

}
package com.gushultaras.zim.ui.adapter;

import android.view.View;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public interface Clicker {

    void onItemClick(View view);

}

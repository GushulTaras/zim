package com.gushultaras.zim.ui.data;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gushultaras.zim.data.Data;
import com.gushultaras.zim.interactor.DataInteractor;
import com.gushultaras.zim.interactor.IDataInteractor;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public class DataFragmentVM extends ViewModel {

    public final ObservableBoolean refreshing = new ObservableBoolean();

    private IDataInteractor interactor;
    private CompositeDisposable compositeDisposable;

    private MutableLiveData<List<Data>> items = new MutableLiveData<>();
    private String parameter;

    public DataFragmentVM() {
        interactor = new DataInteractor();
        compositeDisposable = new CompositeDisposable();
    }

    public void bind(String parameter) {
        this.parameter = parameter;
    }

    public void setRestoredList(List<Data> list) {
        items.setValue(list);
    }

    public void loadData() {
        if (items.getValue() != null) {
            return;
        }
        refreshData();
    }

    public void refreshData() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed() && interactor != null) {
            refreshing.set(true);
            compositeDisposable.add(interactor.getDataList(parameter)
                    .subscribe(data -> {
                        items.setValue(data);
                        refreshing.set(false);
                    }, throwable -> {
                        refreshing.set(false);
                        throwable.printStackTrace();
                    }));
        }
    }

    public MutableLiveData<List<Data>> getItems() {
        return items;
    }

    @Override
    protected void onCleared() {
        compositeDisposable.dispose();
        compositeDisposable = null;
        interactor = null;
        super.onCleared();
    }

}

package com.gushultaras.zim.ui.host;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.gushultaras.zim.R;
import com.gushultaras.zim.data.Data;
import com.gushultaras.zim.network.Parameter;
import com.gushultaras.zim.ui.data.DataFragment;
import com.gushultaras.zim.ui.details.DetailsFragment;

public class MainActivity extends AppCompatActivity implements BottomNavigationVisibilityListener, NavigationClickListener {

    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            setupBottomNavigationBar(null);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        setupBottomNavigationBar(savedInstanceState);
    }

    private void setupBottomNavigationBar(Bundle savedInstanceState) {
        bottomNavigationView = findViewById(R.id.bottom_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.cats:
                    attachFragment(DataFragment.CATS_FRAGMENT_TAG, Parameter.CATS);
                    return true;
                case R.id.dogs:
                    attachFragment(DataFragment.DOGS_FRAGMENT_TAG, Parameter.DOGS);
                    return true;
                default:
                    return false;
            }
        });
        if (savedInstanceState == null) {
            bottomNavigationView.setSelectedItemId(R.id.cats);
        }
        bottomNavigationView.setOnNavigationItemReselectedListener(item -> {
        });
    }

    private void attachFragment(String tag, String parameter) {
        detachCurrentFragment();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .attach(fragment)
                    .commit();
        } else {
            Fragment dataFragment = DataFragment.newInstance(parameter);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, dataFragment, tag)
                    .commit();
        }
    }

    private void detachCurrentFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .detach(fragment)
                    .commit();
        }
    }

    @Override
    public void show() {
        if (bottomNavigationView != null) {
            bottomNavigationView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hide() {
        if (bottomNavigationView != null) {
            bottomNavigationView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        bottomNavigationView = null;
        super.onDestroy();
    }

    @Override
    public void onNavigationClick(Data data) {
        replaceFragment(data);
    }

    private void replaceFragment(Data data) {
        DetailsFragment detailsFragment = DetailsFragment.newInstance(data);
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.container, detailsFragment, "DetailsFragment")
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (bottomNavigationView != null) {
            if (bottomNavigationView.getVisibility() == View.VISIBLE) {
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

}

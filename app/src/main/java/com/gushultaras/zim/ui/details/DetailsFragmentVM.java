package com.gushultaras.zim.ui.details;

import androidx.databinding.ObservableField;
import androidx.lifecycle.ViewModel;

import com.gushultaras.zim.data.Data;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public class DetailsFragmentVM extends ViewModel {

    public final ObservableField<String> pictureUrl = new ObservableField<>();
    public final ObservableField<String> title = new ObservableField<>();

    public void bind(Data data) {
        if (data != null) {
            pictureUrl.set(data.getUrl());
            title.set(data.getTitle());
        }
    }

}

package com.gushultaras.zim.ui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.gushultaras.zim.data.Data;
import com.gushultaras.zim.databinding.ItemDataBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public class DataAdapter extends RecyclerView.Adapter<DataVH> {

    private final OnItemClicker clicker;
    private final List<DataVM> items = new ArrayList<>();

    public DataAdapter(OnItemClicker clicker) {
        this.clicker = clicker;
    }

    @NonNull
    @Override
    public DataVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemDataBinding binding = ItemDataBinding.inflate(inflater, parent, false);
        return new DataVH(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull DataVH holder, int position) {
        DataVM vm = items.get(position);
        holder.getBinding().setItem(vm);
        holder.getBinding().setClick(view -> clicker.onItemClick(vm.getData()));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItems(List<Data> list) {
        List<DataVM> vms = new ArrayList<>();
        for (Data data : list) {
            vms.add(new DataVM(data));
        }

        DiffUtilCallback callback = new DiffUtilCallback(items, vms);
        DiffUtil.DiffResult result = DiffUtil.calculateDiff(callback);

        this.items.clear();
        this.items.addAll(vms);
        result.dispatchUpdatesTo(this);
    }

}

package com.gushultaras.zim.ui.data;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gushultaras.zim.R;
import com.gushultaras.zim.data.Data;
import com.gushultaras.zim.databinding.FragmentDataBinding;
import com.gushultaras.zim.ui.adapter.DataAdapter;
import com.gushultaras.zim.ui.host.NavigationClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public class DataFragment extends Fragment {

    public static final String CATS_FRAGMENT_TAG = "CatsFragment";
    public static final String DOGS_FRAGMENT_TAG = "DogsFragment";

    private static final String DATA_ARG = "DATA_ARG";
    private static final String LIST_DATA_ARG = "LIST_DATA_ARG";

    private FragmentDataBinding binding;
    private DataFragmentVM vm;
    private DataAdapter adapter;
    private NavigationClickListener listener;

    public static DataFragment newInstance(String parameter) {
        Bundle args = new Bundle();
        args.putString(DATA_ARG, parameter);
        DataFragment fragment = new DataFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        if (context instanceof NavigationClickListener) {
            listener = (NavigationClickListener) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_data, container, false);
        View view = binding.getRoot();
        vm = ViewModelProviders.of(this).get(DataFragmentVM.class);
        binding.setVm(vm);
        return view;
    }

    private void handleArguments(ArrayList<Data> savedData) {
        Bundle args = getArguments();
        if (args != null) {
            String parameter = args.getString(DATA_ARG);
            vm.bind(parameter);
            if (savedData == null) {
                vm.loadData();
            } else {
                vm.setRestoredList(savedData);
            }
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (vm != null) {
            outState.putParcelableArrayList(LIST_DATA_ARG, (ArrayList<? extends Parcelable>) vm.getItems().getValue());
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ArrayList<Data> savedData = null;
        if (savedInstanceState != null) {
            savedData = savedInstanceState.getParcelableArrayList(LIST_DATA_ARG);
        }
        handleArguments(savedData);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new DataAdapter(data -> {
            if (listener != null) {
                listener.onNavigationClick(data);
            }
        });
        initRecyclerView();
        subscribeData();
    }

    private void initRecyclerView() {
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        binding.recyclerView.setLayoutManager(manager);
        binding.recyclerView.setAdapter(adapter);
        binding.swipeRefreshLayout.setOnRefreshListener(() -> vm.refreshData());
    }

    private void subscribeData() {
        vm.getItems().observe(this, data -> {
            adapter.setItems(data);
        });
    }

    @Override
    public void onDetach() {
        listener = null;
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        adapter = null;
        binding = null;
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        vm = null;
        super.onDestroy();
    }
}

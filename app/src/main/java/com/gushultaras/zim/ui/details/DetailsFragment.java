package com.gushultaras.zim.ui.details;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.gushultaras.zim.R;
import com.gushultaras.zim.data.Data;
import com.gushultaras.zim.databinding.FragmentDetailsBinding;
import com.gushultaras.zim.ui.host.BottomNavigationVisibilityListener;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public class DetailsFragment extends Fragment {

    public static final String DATA_ARG = "DATA_ARG";

    private FragmentDetailsBinding binding;
    private DetailsFragmentVM vm;
    private BottomNavigationVisibilityListener listener;

    public static DetailsFragment newInstance(Data data) {
        Bundle args = new Bundle();
        args.putParcelable(DATA_ARG, data);
        DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        if (context instanceof BottomNavigationVisibilityListener) {
            listener = (BottomNavigationVisibilityListener) context;
        }
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details, container, false);
        View view = binding.getRoot();
        vm = ViewModelProviders.of(this).get(DetailsFragmentVM.class);
        binding.setVm(vm);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.toolbar.setNavigationOnClickListener(v -> {
            if (getActivity() != null) getActivity().onBackPressed();
        });
        handleArguments();
    }

    private void handleArguments() {
        Bundle args = getArguments();
        if (args != null) {
            vm.bind(args.getParcelable(DATA_ARG));
        }
    }

    @Override
    public void onDestroyView() {
        binding = null;
        vm = null;
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (listener != null) {
            listener.hide();
        }
    }

    @Override
    public void onDetach() {
        if (listener != null) {
            listener.show();
        }
        listener = null;
        super.onDetach();
    }

}

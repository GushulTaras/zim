package com.gushultaras.zim.ui.host;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public interface BottomNavigationVisibilityListener {

    void show();

    void hide();

}

package com.gushultaras.zim.interactor;

import com.gushultaras.zim.data.Data;

import java.util.List;

import io.reactivex.Single;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public interface IDataInteractor {

    Single<List<Data>> getDataList(String id);

}

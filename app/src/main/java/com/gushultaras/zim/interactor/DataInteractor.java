package com.gushultaras.zim.interactor;

import com.gushultaras.zim.data.Data;
import com.gushultaras.zim.network.repository.DataRepository;
import com.gushultaras.zim.network.repository.IDataRepository;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public class DataInteractor implements IDataInteractor {

    private IDataRepository dataRepository;

    public DataInteractor() {
        dataRepository = new DataRepository();
    }

    @Override
    public Single<List<Data>> getDataList(String id) {
        return dataRepository.getDataList(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}

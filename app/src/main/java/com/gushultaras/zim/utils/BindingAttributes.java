package com.gushultaras.zim.utils;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.gushultaras.zim.R;
import com.squareup.picasso.Picasso;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public class BindingAttributes {

    @BindingAdapter("pictureUrl")
    public static void setPictureUrl(ImageView imageView, String pictureUrl) {
        Picasso.get().load(pictureUrl).placeholder(R.drawable.bg_no_picture).into(imageView);
    }

}

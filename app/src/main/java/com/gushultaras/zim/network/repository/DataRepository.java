package com.gushultaras.zim.network.repository;

import com.gushultaras.zim.data.Data;
import com.gushultaras.zim.network.api.ApiFactory;
import com.gushultaras.zim.network.dto.DataDTO;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public class DataRepository implements IDataRepository {

    @Override
    public Single<List<Data>> getDataList(String id) {
        return ApiFactory.getService().getDataList(id)
                .toObservable()
                .map(it -> it.getData() == null ? new ArrayList<DataDTO>() : it.getData())
                .map(this::filterNullItems)
                .flatMapIterable(it -> it)
                .map(DataDTO::toData)
                .toList();
    }

    private List<DataDTO> filterNullItems(List<DataDTO> dataDTOS) {
        List<DataDTO> data = new ArrayList<>();
        for (DataDTO dto : dataDTOS) {
            if (dto != null) {
                data.add(dto);
            }
        }
        return data;
    }

}

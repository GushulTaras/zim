package com.gushultaras.zim.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.gushultaras.zim.data.Data;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public class DataDTO {

    @SerializedName("url")
    @Expose
    private String url;

    @SerializedName("title")
    @Expose
    private String title;

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public Data toData() {
        return new Data(url, title);
    }

}

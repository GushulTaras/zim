package com.gushultaras.zim.network.api;

import com.gushultaras.zim.network.Endpoint;
import com.gushultaras.zim.network.dto.DataListDTO;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public interface ApiService {

    @GET(Endpoint.DATA)
    Single<DataListDTO> getDataList(@Query("query") String query);

}

package com.gushultaras.zim.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-19
 */
public class DataListDTO {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<DataDTO> data;

    public String getMessage() {
        return message;
    }

    public List<DataDTO> getData() {
        return data;
    }

}

package com.gushultaras.zim.network;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Date: 2019-07-22
 */
public interface Parameter {

    String CATS = "cat";
    String DOGS = "dog";

}
